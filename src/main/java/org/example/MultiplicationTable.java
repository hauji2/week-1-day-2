package org.example;

public class MultiplicationTable {

    public String printMultiplicationTable(int start, int end) {
        String result = "";

        if (!isStartAndEndInRange(start, end) || !isStartSmallerOrEqualThanEnd(start, end)) {
            return null;
        }

        return printTableResult(start, end);
    }

    protected boolean isStartSmallerOrEqualThanEnd(int start, int end) {
        return start <= end;
    }

    protected boolean isStartAndEndInRange(int start, int end) {
        // check start and end is in 1 to 1000 range
        boolean isStartInRange = start >= 1 && start <= 1000;
        boolean isEndInRange = end >= 1 && end <= 1000;
        return isStartInRange && isEndInRange;
    }

    protected String printTableResult(int start, int end) {
        StringBuilder result = new StringBuilder();
        for (int i = start; i <= end; i++) {
            for (int j = start; j <= i; j++) {
                result.append(String.format("%d*%d", i, j)).append("=").append(i * j);
                if (j < i) {
                    result.append(" ");
                }
            }
            if (i < end) {
                result.append("\n");
            }
        }
        return result.toString();
    }
}
