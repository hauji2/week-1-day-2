package org.example;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MultiplicationTableTest {

    MultiplicationTable multiplicationTable = new MultiplicationTable();

    @Test
    public void test_isStartSmallerOrEqualThanEnd_should_return_true_when_start_is_smaller_or_equal_to_end() {
        Integer start = 1;
        Integer end = 10;
        boolean result = multiplicationTable.isStartSmallerOrEqualThanEnd(start, end);

        assertTrue(result);
    }

    @Test
    public void test_isStartSmallerOrEqualThanEnd_should_return_false_when_start_is_greater_than_end() {
        Integer start = 11;
        Integer end = 10;
        boolean result = multiplicationTable.isStartSmallerOrEqualThanEnd(start, end);

        assertFalse(result);
    }

    @Test
    public void test_isStartAndEndInRange_should_return_true_when_start_and_end_are_in_range() {
        Integer start = 1;
        Integer end = 10;
        boolean result = multiplicationTable.isStartAndEndInRange(start, end);

        assertTrue(result);
    }

    @Test
    public void test_isStartAndEndInRange_should_return_false_when_start_or_end_is_not_in_range() {
        // case 1: start is not in range
        Integer start = -1;
        Integer end = 10;
        boolean result = multiplicationTable.isStartAndEndInRange(start, end);
        assertFalse(result);

         start = 10000;
         end = 10;
        result = multiplicationTable.isStartAndEndInRange(start, end);
        assertFalse(result);

        // case 2: end is not in range
        start = 1;
        end = -10;
        result = multiplicationTable.isStartAndEndInRange(start, end);
        assertFalse(result);

        start = 1;
        end = 10002;
        result = multiplicationTable.isStartAndEndInRange(start, end);
        assertFalse(result);
    }

    @Test
    public void test_printTableResult() {
        Integer start = 2;
        Integer end = 4;

        String result = multiplicationTable.printTableResult(start, end);
        System.out.println(result);
    }

    @Test
    public void test_printMultiplicationTable() {
        Integer start = 2;
        Integer end = 4;

        String result = multiplicationTable.printMultiplicationTable(start, end);
        System.out.println(result);
    }

    @Test
    public void test_printMultiplicationTable_should_return_null() {
        Integer start = 5;
        Integer end = 4;

        String result = multiplicationTable.printMultiplicationTable(start, end);
        assertNull(result);
    }


}
